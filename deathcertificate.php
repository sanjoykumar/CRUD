<?php

function debug($aVariable){
   
    if(is_array($aVariable)){
        echo "<pre>";
        print_r( $aVariable );
        echo "</pre>";
    }else{
        var_dump($aVariable);
    }   
}

debug($_POST);


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Understanding CRUD - with Sample US Death Certificate Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>
           
                <h1>Appendix A - Sample US Death Certificate Form</h1>
                <p>The sample death reporting form ....</p>
                <h2>Death Reporting For Vital Records</h2>
                
                <fieldset>
                    <legend>Decedent's Name(Include AKA's if any)</legend>
                    
                    <ul>
                        <li>
                            <label for="lastName">Last Name:</label>
                            <?php
                                if(array_key_exists('lastname',$_POST) && !empty($_POST['lastname'])){
                                    echo $_POST['lastname'];
                                }else{
                                    echo "Last Name Not Provided";
                                }
                            ?>
                        </li>
                        

                        <li>
                            <label for="firstName">First Name:</label>
                            <?php
                                if(array_key_exists('firstname',$_POST) && !empty($_POST['firstname'])){
                                    echo $_POST['firstname'];
                                }else{
                                    echo "First Name Not Provided";
                                }
                            ?>
                        </li>

                        <li>
                            <label for="middleName">Middle Name:</label>
                            <?php
                                if(array_key_exists('middlename',$_POST) && !empty($_POST['middlename'])){
                                    echo $_POST['middlename'];
                                }else{
                                    echo "Middle name Not Provided";
                                }
                            ?>
                        </li>
                        
                        <li>
                            <label for="DateOfBirth">Date of Birth:</label>
                            <?php
                                if(array_key_exists('dob',$_POST) && !empty($_POST['dob'])){
                                    echo $_POST['dob'];
                                }else{
                                    echo "dob Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="Gender">Gender:</label>
                            <?php
                                if(array_key_exists('gender',$_POST) && !empty($_POST['gender'])){
                                    echo $_POST['gender'];
                                }else{
                                    echo "Gender Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="SCN">Social Security Number:</label>
                            <?php
                                if(array_key_exists('scn',$_POST) && !empty($_POST['scn'])){
                                    echo $_POST['scn'];
                                }else{
                                    echo "SCN Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="FacilityName">Facility Name:</label>
                            <?php
                                if(array_key_exists('facilityname',$_POST) && !empty($_POST['facilityname'])){
                                    echo $_POST['facilityname'];
                                }else{
                                    echo "Facility Name Not Provided";
                                }
                            ?>
                        </li>
                    </ul>
                </fieldset>    
                
                                
                <fieldset>
                    <legend>Decedent of Hispanic Origin</legend>
                    
                    <?php
                        if(array_key_exists('origin',$_POST) && !empty($_POST['origin'])){
                            echo $_POST['origin'];
                        }elseif(array_key_exists('originother',$_POST) && !empty($_POST['originother'])){
                            echo $_POST['originother'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset> 
                
                <fieldset>
                    <legend>Decedent's Race</legend>
                    
                    <?php
                        if(array_key_exists('race',$_POST) && !empty($_POST['race'])){
                            echo $_POST['race'];
                        }elseif(array_key_exists('racetext',$_POST) && !empty($_POST['racetext'])){
                            echo $_POST['racetext'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset> 
               
			   <fieldset>
                    <legend>Pronounces or Certificate Death:</legend>

                    <ul>
                        <li>
                            <label for="datepronounced">Date Pronounced Dead:</label>
                            
                            <?php
                                if(array_key_exists('datepronounced',$_POST) && !empty($_POST['datepronounced'])){
                                    echo $_POST['datepronounced'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        

                        <li>
                            <label for="lnumber">License Number:</label>
                            <?php
                                if(array_key_exists('lnumber',$_POST) && !empty($_POST['lnumber'])){
                                    echo $_POST['lnumber'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>

                        <li>
                            <label for="aptod">Actual or Presumed Time of Death:</label>
                            <?php
                                if(array_key_exists('aptod',$_POST) && !empty($_POST['aptod'])){
                                    echo $_POST['aptod'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        
                        <li>
                           <label for="tpd">Time Pronounced Dead:</label>
                            <?php
                                if(array_key_exists('tpd',$_POST) && !empty($_POST['tpd'])){
                                    echo $_POST['tpd'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="datesigned">Date Signed:</label>
                            <?php
                                if(array_key_exists('datesigned',$_POST) && !empty($_POST['datesigned'])){
                                    echo $_POST['datesigned'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="medicalexam">Was Medical Examiner or Corner Contacted?</label>
                            <?php
                                if(array_key_exists('medicalexam',$_POST) && !empty($_POST['medicalexam'])){
                                    echo $_POST['medicalexam'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="signatureofperson">Signature Of Person Pronouncing Death:</label>
                            <?php
                                if(array_key_exists('signatureofperson',$_POST) && !empty($_POST['signatureofperson'])){
                                    echo $_POST['signatureofperson'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="actualdob">Actual Or Presumed Date of Birth:</label>
                            <?php
                                if(array_key_exists('actualdob',$_POST) && !empty($_POST['actualdob'])){
                                    echo $_POST['actualdob'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                    </ul>
                </fieldset>    
           <fieldset>
                    <legend>Cause of  Death:</legend>

                    <ul>
                        <li>
                            <label for="immediatecause">a.Immediate Cause (Final disease or condition resulting in death)</label>
                            <?php
                                if(array_key_exists('immediatecause',$_POST) && !empty($_POST['immediatecause'])){
                                    echo $_POST['immediatecause'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
							
							</li>

                        <li>
                            <label for="listconditiona"> b.Sequentially List Conditions,(if any, leading to the cause listed on line a.):</label>
                            <?php
                                if(array_key_exists('listconditiona',$_POST) && !empty($_POST['listconditiona'])){
                                    echo $_POST['listconditiona'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
							</li>
                        <li>
                            <label for="underlyingcause">c.Enter the <b> Underlying Cause</b>,(disease or injury that initiated the events resulting in death)</label>
                            <?php
                                if(array_key_exists('underlyingcause',$_POST) && !empty($_POST['underlyingcause'])){
                                    echo $_POST['underlyingcause'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="last">d.Last</label>
                            <?php
                                if(array_key_exists('last',$_POST) && !empty($_POST['last'])){
                                    echo $_POST['last'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>

                        <li>
                            <label for="dueto1">Due to (or as a consequence of):</label>
                           <?php
                                if(array_key_exists('dueto1',$_POST) && !empty($_POST['dueto1'])){
                                    echo $_POST['dueto1'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="dueto2">Due to (or as a consequence of):</label>
                           <?php
                                if(array_key_exists('dueto2',$_POST) && !empty($_POST['dueto2'])){
                                    echo $_POST['dueto2'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="dueto3">Due to (or as a consequence of):</label>
                           <?php
                                if(array_key_exists('dueto3',$_POST) && !empty($_POST['dueto3'])){
                                    echo $_POST['dueto3'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                        <li>
                            <label for="onsettodeath1">Onset to Death:</label>
                           <?php
                                if(array_key_exists('onsettodeath1',$_POST) && !empty($_POST['onsettodeath1'])){
                                    echo $_POST['onsettodeath1'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="onsettodeath2">Onset to Death:</label>
                            <?php
                                if(array_key_exists('onsettodeath2',$_POST) && !empty($_POST['onsettodeath2'])){
                                    echo $_POST['onsettodeath2'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="onsettodeath3">Onset to Death:</label>
                            <?php
                                if(array_key_exists('onsettodeath3',$_POST) && !empty($_POST['onsettodeath3'])){
                                    echo $_POST['onsettodeath3'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
						<li>
                            <label for="onsettodeath4">Onset to Death:</label>
                            <?php
                                if(array_key_exists('onsettodeath4',$_POST) && !empty($_POST['onsettodeath4'])){
                                    echo $_POST['onsettodeath4'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
                        </li>
                    </ul>
                </fieldset>  

                <h3>
<p><b>PART II.</b>Enter other significant conditions contributing to death but not resulting in the underlying cause given in <b> PART I</p>
                </h3>
				
                             <?php
                                if(array_key_exists('message',$_POST) && !empty($_POST['message'])){
                                    echo $_POST['message'];
                                }else{
                                    echo "Not Provided";
                                }
                            ?>
               
				
				
                <fieldset>
                    <legend>PART II:</legend>

                    <?php
                        if(array_key_exists('autospy',$_POST) && !empty($_POST['autospy'])){
                            echo $_POST['autospy'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset> 
				
				<fieldset>
                    <legend>If Female:</legend>

					<?php
                        if(array_key_exists('female',$_POST) && !empty($_POST['female'])){
                            echo $_POST['female'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                    
                </fieldset> 
				
				<fieldset>
                    <legend>Manner of Death:</legend>

                  
					<?php
                        if(array_key_exists('mannerofdeath',$_POST) && !empty($_POST['mannerofdeath'])){
                            echo $_POST['mannerofdeath'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset> 
				<fieldset>
                    <legend>Injury:</legend>

                    <ul>
                        <li>
                            <label for="dateofinjury">Date Of Injury:</label>

							<?php
                        if(array_key_exists('dateofinjury',$_POST) && !empty($_POST['dateofinjury'])){
                            echo $_POST['dateofinjury'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
							
                        </li>

                        <li>
                            <label for="timeofinjury">Time Of Injury:</label>
                            <?php
                        if(array_key_exists('timeofinjury',$_POST) && !empty($_POST['timeofinjury'])){
                            echo $_POST['timeofinjury'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="placeofinjury">Place Of Injury:</label>
                            <?php
                        if(array_key_exists('placeofinjury',$_POST) && !empty($_POST['placeofinjury'])){
                            echo $_POST['placeofinjury'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="injuryatwork">Injury At Work?</label>
                            
							<?php
                        if(array_key_exists('injuryatwork',$_POST) && !empty($_POST['injuryatwork'])){
                            echo $_POST['injuryatwork'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
	                    </li>
					
						</ul>
                </fieldset>
				
				<fieldset>
                    <legend>Location of Injury:</legend>

                    <ul>
                        <li>
                            <label for="state">State:</label>
                            <?php
                        if(array_key_exists('state',$_POST) && !empty($_POST['state'])){
                            echo $_POST['state'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>

                        <li>
                            <label for="cityortown">City Or Town:</label>
                            <?php
                        if(array_key_exists('cityortown',$_POST) && !empty($_POST['cityortown'])){
                            echo $_POST['cityortown'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="streetnumber">Street and Number:</label>
                            <?php
                        if(array_key_exists('streetnumber',$_POST) && !empty($_POST['streetnumber'])){
                            echo $_POST['streetnumber'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="apartmentnumber">Apartment No:</label>
                            <?php
                        if(array_key_exists('apartmentnumber',$_POST) && !empty($_POST['apartmentnumber'])){
                            echo $_POST['apartmentnumber'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="zipcode">Zip Code:</label>
                            <?php
                        if(array_key_exists('zipcode',$_POST) && !empty($_POST['zipcode'])){
                            echo $_POST['zipcode'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
					
						</ul>
                </fieldset>
				<h3>
                   Describe How Injury Occurred:
				</h3>
                <?php
                        if(array_key_exists('messagehow',$_POST) && !empty($_POST['messagehow'])){
                            echo $_POST['messagehow'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
				<fieldset>
                    <legend>If Transportation Injury:</legend>

                    
					<?php
                        if(array_key_exists('transportationinjury',$_POST) && !empty($_POST['transportationinjury'])){
                            echo $_POST['transportationinjury'];
                        }elseif(array_key_exists('transportationinjurytext',$_POST) && !empty($_POST['transportationinjurytext'])){
                            echo $_POST['transportationinjurytext'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset>	
				<h3>Certifier</h3>
                            <hr>
				<fieldset>
                    <legend>Check Only One:</legend>

                    
					<?php
                        if(array_key_exists('Certifier',$_POST) && !empty($_POST['Certifier'])){
                            echo $_POST['Certifier'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
					<?php
                        if(array_key_exists('Certifiertext',$_POST) && !empty($_POST['Certifiertext'])){
                            echo $_POST['Certifiertext'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                </fieldset>	
				
				<h3>Person Completing Cause of Death:</h3>
                                        <hr>
				<fieldset>
                    <legend>Person Completing Cause of Death:</legend>

                    <ul>

					<li>
                            <label for="personname">Name:</label>
                            <?php
                        if(array_key_exists('personname',$_POST) && !empty($_POST['personname'])){
                            echo $_POST['personname'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>

                        <li>
                            <label for="address">Address:</label>
                            <?php
                        if(array_key_exists('address',$_POST) && !empty($_POST['address'])){
                            echo $_POST['address'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="zip_code">ZIP Code:</label>
                            <?php
                        if(array_key_exists('zip_code',$_POST) && !empty($_POST['zip_code'])){
                            echo $_POST['zip_code'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="titleofcertifier">Title of Certifier:</label>
                            <?php
                        if(array_key_exists('titleofcertifier',$_POST) && !empty($_POST['titleofcertifier'])){
                            echo $_POST['titleofcertifier'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
                        <li>
                            <label for="licensenumber">License Number:</label>
                            <?php
                        if(array_key_exists('licensenumber',$_POST) && !empty($_POST['licensenumber'])){
                            echo $_POST['licensenumber'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
						<li>
                            <label for="DateCertified">Date Certified:</label>
                            <?php
                        if(array_key_exists('DateCertified',$_POST) && !empty($_POST['DateCertified'])){
                            echo $_POST['DateCertified'];
                        }
                        else{
                            echo "Not Provided";
                        }
                    ?>
                        </li>
					
					</ul>
                </fieldset>	
				
        </section>
    </body>
</html>
