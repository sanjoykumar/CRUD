<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h3>Text Input</h3>
        <form>
            First name:<br>
            <input type="text" name="firstname">
            <br>
            Last name:<br>
            <input type="text" name="lastname">
        </form>

        <h3>Radio Button Input</h3>
        
        <form>
            <input type="radio" name="sex" value="male" checked>Male
            <br>
            <input type="radio" name="sex" value="female">Female
        </form>

        <h3>The Submit Button</h3>
        
        <form action="action_page.php">
            First name:<br>
            <input type="text" name="firstname" value="Mickey">
            <br>
            Last name:<br>
            <input type="text" name="lastname" value="Mouse">
            <br><br>
            <input type="submit" value="Submit">
        </form>

        <h3>Grouping Form Data with &lt;fieldset&gt;</h3>
        
        <form action="action_page.php">
            <fieldset>
                <legend>Personal information:</legend>
                First name:<br>
                <input type="text" name="firstname" value="Mickey">
                <br>
                Last name:<br>
                <input type="text" name="lastname" value="Mouse">
                <br><br>
                <input type="submit" value="Submit"></fieldset>
        </form>

        
        <h3>HTML5 &lt;datalist&gt; Element</h3>
        <form action="action_page.php">
            <input list="browsers">
            <datalist id="browsers">
                <option value="Internet Explorer">
                <option value="Firefox">
                <option value="Chrome">
                <option value="Opera">
                <option value="Safari">
            </datalist> 
        </form>

    </body>
</html>
